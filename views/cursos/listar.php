<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CursosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de todos nuestros cursos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cursos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'nombre',
            'especialidad',
            'nivel',
            'dirigido',
            'modalidad',
            'turno',
            // mostrar una imagen dentro del gridview
            [
                'attribute' => 'cartel',
                'format' => 'image',
                'value' => function ($model) {
                    return $model->getCartel('cartel');
                },
                'contentOptions' => ['class' => 'grid-imagen'],
            ],
            'informacion',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
